import bodyParser from "body-parser";
import express from "express";
import helmet from "helmet";
import morgan from "morgan";

import { NextFunction, Request, Response } from "express";
import HttpException from "./exceptions/http-exceptions";
import { HTTPCode } from "./http-code";
import marvelRouter from "./routes/marvel-route";

// get env vars, default 3000.
const port = +(process.env.PORT || 3000);

/**
 * Express settings
 */
const app: express.Application = express();

app.use((request: Request, response: Response, nextFunction: NextFunction) => {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    nextFunction();
});

app.use(morgan("tiny")); // log API request
app.use(helmet()); // secure context
app.use(bodyParser.json({ limit: "1Mb" }));

// ping route
app.get("/ping", (req, res) => {
    res.status(HTTPCode.Success).send("PONG");
});

// marvel route
app.use("/marvel", marvelRouter);

// route not found, catch BadRequest and forward to error handler
app.use((request: Request, response: Response, nextFunction: NextFunction) => {
    nextFunction(new HttpException(HTTPCode.BadRequest));
});

// error handling
app.use((error: HttpException, request: Request, response: Response, nextFunction: NextFunction) => {
    const status = error.status || HTTPCode.InternalServerError;
    const message = error.message || "Something went wrong";
    response
        .status(status)
        .send({
            status,
            message,
        });
});

app.listen(port, () => { console.log(`Server is listening on port ${port}`); });
