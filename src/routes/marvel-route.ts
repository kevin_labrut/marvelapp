import express, { NextFunction, Request, Response } from "express";
import HttpException from "../exceptions/http-exceptions";
import { HTTPCode } from "../http-code";
import { MarvelService } from "../services/marvel.service";

const router = express.Router();

/* First method to determine the buyer */
router.get("/getAll", (request: Request, response: Response, nextFunction: NextFunction) => {
    // validate request
    if (validateMarvelRequest(request)) {
        const page: number = Number(request.query.page);
        // TO DO singleton service
        const marvelService: MarvelService = new MarvelService();

        // get the list marvel
        marvelService.getList(page).then((result) => {
            response.status(HTTPCode.Success).send(result);
        });
    } else {
        nextFunction(new HttpException(HTTPCode.BadRequest, "Bad request :'("));
    }
});

function validateMarvelRequest(request: Request) {
    if (!request.query) {
        return false;
    }
    // page is required
    if (!request.query.page || typeof request.query.page !== "string") {
        return false;
    }

    return true;
}

export default router;
