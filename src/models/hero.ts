export interface IHero {
    id: string;
    name: string;
    description: string;
    modified: Date;
    resourceURI: string;
    urls: string[];
    thumbnail: {
        path: string;
        extension: string;
    };
    // comics	ResourceList
    // stories	ResourceList
    // events	ResourceList
    // series	ResourceList
}
