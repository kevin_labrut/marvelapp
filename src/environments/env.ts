const env = {
    marvelPublicKey: process.env.marvelPublicKey || "23ce9156b7c6e36b57b7ddcb6571c475",
    marvelPrivateKey: process.env.marvelPrivateKey ||  "8960c893732971128345845f46b71aeddefcd23c",
    marvelEndpoint: "http://gateway.marvel.com",
    limit: 20,
};

export default env;
