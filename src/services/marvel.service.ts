
import axios from "axios";
import md5 from "md5";
import querystring from "querystring";

import env from "../environments/env";
import { IHero } from "../models";

interface IGetListHero {
    results: IHero[];
    total: number;
}

export class MarvelService {

    public getList(page: number) {
        const offset = env.limit * (page - 1);
        const params: string = this.getTsAndHashCode(offset, env.limit);
        return new Promise<IGetListHero>((resolve, reject) => {
            axios.get(`${env.marvelEndpoint}/v1/public/characters?${params}`).then((resp) => {
                const heroes: IHero[] = resp.data.data.results;
                const total = resp.data.data.total;
                resolve({ results: heroes, total });
            });
        });
    }

    private getTsAndHashCode(offset: number, limit: number): string {
        const ts: number = new Date().getTime();
        const hash: string = md5(ts + env.marvelPrivateKey + env.marvelPublicKey);
        const apikey: string = env.marvelPublicKey;
        return querystring.encode({ ts, hash, apikey, offset, limit });
    }
}
