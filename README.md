# MarvelApp
## Introduction

**MarvelApp** is server-side solution of the project TEST TravelCar.

NO Architectural Board :)

## Pre-requirements

You should have Node.js install on your system (https://nodejs.org/en/).

## Tests

No test. 

TODO: create a api test and loadtest with postman/newman

## Build

First install all dependencies :
```sh
npm install
```

You must build the project with :
```sh
npm run build
```

You have to launch the server with :
```sh
npm run start
```

## Deploy docker

First build the docker image :
```sh
docker build -t marvel:back .
```

You have to run the image with :
```sh
docker run -d -p 3000:3000 --name=marvel-back marvel:back
```
enjoy :)

## Authors
Kevin LABRUT (kevin.labrut@gmail.com) - version: 1.0.0
