FROM node:10.15.3-alpine
ENV PORT=3000
ENV marvelPublicKey=23ce9156b7c6e36b57b7ddcb6571c475
ENV marvelPrivateKey=8960c893732971128345845f46b71aeddefcd23c

WORKDIR /usr/src/app
COPY package*.json ./

RUN npm install

COPY src ./src
COPY ts*.json ./

RUN npm run build

EXPOSE ${PORT}

CMD ["npm", "start"]